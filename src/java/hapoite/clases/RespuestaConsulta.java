package hapoite.clases;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RespuestaConsulta
/*     */ {
/*     */   private String estado;
/*     */   private String mensaje;
/*     */   private String nombreApellido;
/*     */   private String habilitado;
/*     */   private String ultimaAsamblea;
/*     */   private List<Telefonos> telefonos;
/*     */   
/*     */   public RespuestaConsulta()
/*     */   {
/*  25 */     this.estado = "";
/*  26 */     this.nombreApellido = "";
/*  27 */     this.habilitado = "";
/*  28 */     this.ultimaAsamblea = "";
/*  29 */     this.telefonos = new ArrayList();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getEstado()
/*     */   {
/*  36 */     return this.estado;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setEstado(String estado)
/*     */   {
/*  43 */     this.estado = estado;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getMensaje()
/*     */   {
/*  50 */     return this.mensaje;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setMensaje(String mensaje)
/*     */   {
/*  57 */     this.mensaje = mensaje;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getNombreApellido()
/*     */   {
/*  64 */     return this.nombreApellido;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNombreApellido(String nombreApellido)
/*     */   {
/*  71 */     this.nombreApellido = nombreApellido;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getHabilitado()
/*     */   {
/*  78 */     return this.habilitado;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setHabilitado(String habilitado)
/*     */   {
/*  85 */     this.habilitado = habilitado;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getUltimaAsamblea()
/*     */   {
/*  92 */     return this.ultimaAsamblea;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setUltimaAsamblea(String ultimaAsamblea)
/*     */   {
/*  99 */     this.ultimaAsamblea = ultimaAsamblea;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public List<Telefonos> getTelefonos()
/*     */   {
/* 106 */     return this.telefonos;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setTelefonos(List<Telefonos> telefonos)
/*     */   {
/* 113 */     this.telefonos = telefonos;
/*     */   }
/*     */ }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\beans\RespuestaConsulta.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */