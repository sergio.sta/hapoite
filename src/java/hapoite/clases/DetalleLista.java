package hapoite.clases;

import java.sql.Date;

/*     */ 
/*     */ 
/*     */ public class DetalleLista
/*     */ {
/*     */   private String nombreApellidoOperador;
/*     */   
/*     */   private String codigoOperador;
/*     */   
/*     */   private String cedulaCorreli;
/*     */   
/*     */   private String nombreApellidoCorreli;
/*     */   
/*     */   private String habilitado;
/*     */   
/*     */   private String voto;
/*     */   
/*     */   private String celular;
/*     */   
/*     */   private String localVotacion;
/*     */   
/*     */   private Integer nroMesa;
/*     */   
/*     */   private Integer ordenMesa;
/*     */   
/*     */   private Integer nroConsulta;
            private Date aniversario;
/*     */   
/*     */   public String getNombreApellidoOperador()
/*     */   {
/*  30 */     return this.nombreApellidoOperador;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNombreApellidoOperador(String nombreApellidoOperador)
/*     */   {
/*  37 */     this.nombreApellidoOperador = nombreApellidoOperador;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getCodigoOperador()
/*     */   {
/*  44 */     return this.codigoOperador;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setCodigoOperador(String codigoOperador)
/*     */   {
/*  51 */     this.codigoOperador = codigoOperador;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getCedulaCorreli()
/*     */   {
/*  58 */     return this.cedulaCorreli;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setCedulaCorreli(String cedulaCorreli)
/*     */   {
/*  65 */     this.cedulaCorreli = cedulaCorreli;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getNombreApellidoCorreli()
/*     */   {
/*  72 */     return this.nombreApellidoCorreli;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNombreApellidoCorreli(String nombreApellidoCorreli)
/*     */   {
/*  79 */     this.nombreApellidoCorreli = nombreApellidoCorreli;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getHabilitado()
/*     */   {
/*  86 */     return this.habilitado;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setHabilitado(String habilitado)
/*     */   {
/*  93 */     this.habilitado = habilitado;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getVoto()
/*     */   {
/* 100 */     return this.voto;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setVoto(String voto)
/*     */   {
/* 107 */     this.voto = voto;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getCelular()
/*     */   {
/* 114 */     return this.celular;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setCelular(String celular)
/*     */   {
/* 121 */     this.celular = celular;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getLocalVotacion()
/*     */   {
/* 128 */     return this.localVotacion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setLocalVotacion(String localVotacion)
/*     */   {
/* 135 */     this.localVotacion = localVotacion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Integer getNroMesa()
/*     */   {
/* 142 */     return this.nroMesa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNroMesa(Integer nroMesa)
/*     */   {
/* 149 */     this.nroMesa = nroMesa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Integer getOrdenMesa()
/*     */   {
/* 156 */     return this.ordenMesa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setOrdenMesa(Integer ordenMesa)
/*     */   {
/* 163 */     this.ordenMesa = ordenMesa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Integer getNroConsulta()
/*     */   {
/* 170 */     return this.nroConsulta;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNroConsulta(Integer nroConsulta)
/*     */   {
/* 177 */     this.nroConsulta = nroConsulta;
/*     */   }
/*     */

    public void setAniversario(Date date) {
        this.aniversario = date;
    }

    /**
     * @return the aniversario
     */
    public Date getAniversario() {
        return aniversario;
    }
 }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\beans\DetalleLista.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */