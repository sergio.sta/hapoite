/*     */ package hapoite.main;
/*     */ 
import hapoite.utiles.ReadFile;
/*     */ import com.google.gson.Gson;
import hapoite.bd.BD;
import hapoite.clases.RespuestaComun;

import hapoite.utils.StringUtils;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
/*     */ import java.util.HashMap;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/bajacorreli")
/*     */ public class BajaCorreli
/*     */ {
/*     */   private static HashMap params;
private static Logger log = LogManager.getLogger();
/*     */   
/*     */   private static String idLog;
/*  40 */   private final BD db = new BD();
/*  41 */   private final StringUtils StringUtils = new StringUtils();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @GET
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("cedula") String cedula, @QueryParam("imei") String imei, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud)
/*     */     throws Exception
/*     */   {
/*  57 */     Connection conexion = null;
/*  58 */     RespuestaComun respuesta = new RespuestaComun();
/*     */     try
/*     */     {
/*     */       try {
/*  62 */         idLog = "1";
/*     */         
/*  64 */         ReadFile file = new ReadFile();
/*  65 */         params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  67 */         log.error("[" + idLog + "] ALTACORRELI - Error reading configuration  [" + ex.getMessage() + "]");
/*  68 */         respuesta.setEstado("ERROR");
/*  69 */         respuesta.setMensaje("INTERNAL #1");
/*  70 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  73 */       log.info("[" + idLog + "] ## Activacion - Parametros ##");
/*  74 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  75 */       log.info("[" + idLog + "] " + "password [" + password + "]");
/*  76 */       log.info("[" + idLog + "] " + "cedula [" + cedula + "]");
/*  77 */       log.info("[" + idLog + "] " + "imei [" + imei + "]");
/*  78 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  79 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*     */       try
/*     */       {
/*  83 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  85 */         log.error("[" + idLog + "] ALTACORRELI - Error when connecting to database  [" + ex.getMessage() + "]");
/*  86 */         respuesta.setEstado("ERROR");
/*  87 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  88 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  93 */       params.put("Cedula", cedula);
/*  94 */       params.put("IMEI", imei);
/*  95 */       params.put("lat", latitud);
/*  96 */       params.put("long", longitud);
/*     */       
/*     */       try
/*     */       {
/* 100 */         respuesta = ProcesarActivacion(respuesta, idLog, conexion, params);
/*     */       }
/*     */       catch (Exception ex) {
/* 103 */         respuesta.setEstado("ERROR");
/* 104 */         respuesta.setMensaje("INTERNAL #2");
/* 105 */         log.info("[" + idLog + "] ALTACORRELI - Error #2[" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 117 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 119 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 123 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 109 */       if (respuesta.getEstado() == null) {
/* 110 */         respuesta.setEstado("ERROR");
/* 111 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 113 */       log.error("[" + idLog + "] ALTACORRELI - Error Process #3[" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 117 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 119 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 124 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 130 */     Gson gson = new Gson();
/* 131 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 133 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 135 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private RespuestaComun ProcesarActivacion(RespuestaComun respuesta, String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 140 */     ResultSet resultSet = null;
/* 141 */     String query = null;
/*     */     
/*     */     try
/*     */     {
/* 145 */       log.info("[" + idLog_ + "] Preparando la funcion [dba._WebS_Baja]");
/*     */       
/* 147 */       this.stmt = conexion.createStatement();
/* 148 */       query = "SELECT dba._WebS_Baja ('" + params.get("IMEI") + "', '" + params.get("Cedula") + "', '" + params.get("lat") + "', '" + params.get("long") + "') resultado";
/*     */       
/*     */ 
/*     */ 
/* 152 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 153 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 155 */       if (resultSet.next())
/*     */       {
/* 157 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/*     */         
/* 159 */         StringTokenizer tokens = new StringTokenizer(resultSet.getString("resultado"), "#");
/*     */         
/* 161 */         respuesta.setEstado(tokens.nextToken());
/* 162 */         respuesta.setMensaje(this.StringUtils.ReplaceCE(tokens.nextToken()));
/* 163 */         conexion.commit();
/*     */       }
/*     */       else {
/* 166 */         respuesta.setEstado("ERROR");
/* 167 */         respuesta.setMensaje("INTERNAL #4");
/*     */       }
/*     */     }
/*     */     catch (Exception ex) {
/* 171 */       log.error("[" + idLog_ + "]ALTACORRELI - Error #5 [" + ex.getMessage() + "]");
/* 172 */       respuesta.setEstado("ERROR");
/* 173 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }
/* 175 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 176 */     return respuesta;
/*     */   }
/*     */ }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\main\BajaCorreli.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */