/*     */ package hapoite.main;
/*     */ 
import hapoite.utiles.ReadFile;
/*     */ import com.google.gson.Gson;
import hapoite.bd.BD;
import hapoite.clases.RespuestaComun;

import hapoite.utils.StringUtils;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
/*     */ import java.util.HashMap;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/eliminar")
/*     */ public class Eliminar
/*     */ {
/*     */   private static HashMap params;
private static Logger log = LogManager.getLogger();
/*     */   
/*     */   private static String idLog;
/*  40 */   private final BD db = new BD();
/*  41 */   private final StringUtils StringUtils = new StringUtils();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @GET
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("cedula") String cedula, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud)
/*     */     throws Exception
/*     */   {
/*  56 */     Connection conexion = null;
/*  57 */     RespuestaComun respuesta = new RespuestaComun();
/*     */     
/*     */     try
/*     */     {
/*     */       try
/*     */       {
/*  63 */         idLog = "1";
/*     */         
/*  65 */         ReadFile file = new ReadFile();
/*  66 */         params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  68 */         log.error("[" + idLog + "] ELIMINAR -Error reading configuration [" + ex.getMessage() + "]");
/*  69 */         respuesta.setEstado("ERROR");
/*  70 */         respuesta.setMensaje("INTERNAL #1");
/*  71 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  74 */       log.info("[" + idLog + "] ## Activacion - Parametros ##");
/*  75 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  76 */       log.info("[" + idLog + "] " + "password [" + password + "]");
/*  77 */       log.info("[" + idLog + "] " + "cedula [" + cedula + "]");
/*  78 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  79 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*  81 */       log.error("[" + idLog + "] Eliminar: cedula->" + cedula);
/*     */       
/*     */       try
/*     */       {
/*  85 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  87 */         log.error("[" + idLog + "] ELIMINAR -Error when connecting to database  [" + ex.getMessage() + "]");
/*  88 */         respuesta.setEstado("ERROR");
/*  89 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  90 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  95 */       params.put("Cedula", cedula);
/*  96 */       params.put("lat", latitud);
/*  97 */       params.put("long", longitud);
/*     */       
/*     */       try
/*     */       {
/* 101 */         respuesta = ProcesarEliminacion(respuesta, idLog, conexion, params);
/*     */       }
/*     */       catch (Exception ex) {
/* 104 */         respuesta.setEstado("ERROR");
/* 105 */         respuesta.setMensaje("INTERNAL #2");
/* 106 */         log.info("[" + idLog + "] ELIMINAR -Error #2[" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 118 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 120 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 124 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 110 */       if (respuesta.getEstado() == null) {
/* 111 */         respuesta.setEstado("ERROR");
/* 112 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 114 */       log.error("[" + idLog + "] ELIMINAR -Error Process #3 [" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 118 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 120 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 125 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/* 127 */     ByteArrayOutputStream baos = new ByteArrayOutputStream();
/*     */     
/*     */ 
/*     */ 
/* 131 */     Gson gson = new Gson();
/* 132 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 134 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 136 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private RespuestaComun ProcesarEliminacion(RespuestaComun respuesta, String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 141 */     ResultSet resultSet = null;
/* 142 */     String query = null;
/*     */     
/*     */     try
/*     */     {
/* 146 */       log.info("[" + idLog_ + "] Preparando la funcion [dba._WebS_Desactivacion]");
/*     */       
/* 148 */       this.stmt = conexion.createStatement();
/* 149 */       query = "SELECT dba._WebS_Desactivacion ('" + params.get("Cedula") + "', '" + params.get("lat") + "', '" + params.get("long") + "') resultado";
/*     */       
/*     */ 
/* 152 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 153 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 155 */       if (resultSet.next())
/*     */       {
/* 157 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/*     */         
/* 159 */         StringTokenizer tokens = new StringTokenizer(resultSet.getString("resultado"), "#");
/*     */         
/* 161 */         respuesta.setEstado(tokens.nextToken());
/* 162 */         respuesta.setMensaje(tokens.nextToken());
/* 163 */         conexion.commit();
/*     */       }
/*     */       else {
/* 166 */         respuesta.setEstado("ERROR");
/* 167 */         respuesta.setMensaje("INTERNAL #4");
/*     */       }
/*     */     }
/*     */     catch (Exception ex) {
/* 171 */       log.error("[" + idLog_ + "] ELIMINAR -Error #5[" + ex.getMessage() + "]");
/* 172 */       respuesta.setEstado("ERROR");
/* 173 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }
/* 175 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 176 */     return respuesta;
/*     */   }
/*     */ }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\main\Eliminar.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */