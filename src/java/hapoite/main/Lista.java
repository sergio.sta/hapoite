/*     */ package hapoite.main;
/*     */ 
import hapoite.utiles.ReadFile;
import hapoite.clases.DetalleLista;
/*     */ import com.google.gson.Gson;
import hapoite.bd.BD;
import hapoite.clases.RespuestaLista;

import hapoite.utils.StringUtils;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
import java.util.Date;
/*     */ import java.util.HashMap;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;




/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/lista")
/*     */ public class Lista
/*     */ {
/*     */   private static HashMap params;
private static Logger log = LogManager.getLogger();
/*     */   
/*     */   private static String idLog;
/*  37 */   private final BD db = new BD();
/*  38 */   private final StringUtils StringUtils = new StringUtils();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @GET
         
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("imei") String imei, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud)
/*     */     throws Exception
/*     */   {
/*  53 */     Connection conexion = null;
/*  54 */     RespuestaLista respuesta = new RespuestaLista();
/*  55 */     String rPrepararLista = "";
/*     */     try
/*     */     {
/*     */       try {
/*  59 */         idLog = "2";
/*     */         
/*  61 */         ReadFile file = new ReadFile();
/*  62 */         params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  64 */         log.error("[" + idLog + "] LISTA - Error reading configuration [" + ex.getMessage() + "]");
/*  65 */         respuesta.setEstado("ERROR");
/*  66 */         respuesta.setMensaje("INTERNAL #1");
/*  67 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  70 */       log.info("[" + idLog + "] ## Consulta - Parametros ##");
/*  71 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  72 */       log.info("[" + idLog + "] " + "password [" + password + "]");
/*  73 */       log.info("[" + idLog + "] " + "imei [" + imei + "]");
/*  74 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  75 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*     */       try
/*     */       {
/*  79 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  81 */         log.error("[" + idLog + "] LISTA - Error when connecting to database [" + ex.getMessage() + "]");
/*  82 */         respuesta.setEstado("ERROR");
/*  83 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  84 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  89 */       params.put("IMEI", imei);
/*  90 */       params.put("lat", latitud);
/*  91 */       params.put("long", longitud);
/*     */       try
/*     */       {
/*  94 */         rPrepararLista = PrepararLista(idLog, conexion, params);
/*  95 */         StringTokenizer tokens = new StringTokenizer(rPrepararLista, "#");
/*  96 */         respuesta.setEstado(tokens.nextToken());
/*  97 */         respuesta.setNroConsulta(Integer.valueOf(Integer.parseInt(tokens.nextToken())));
/*     */       } catch (Exception ex) {
/*  99 */         respuesta.setEstado("ERROR");
/* 100 */         respuesta.setMensaje("INTERNAL #2");
/* 101 */         log.info("[" + idLog + "] LISTA - Error en la lista #2[" + ex.getMessage() + "]");
/*     */       }
/*     */       
/* 104 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */       
/* 106 */       if (respuesta.getEstado().equals("OK")) {
/* 107 */         respuesta.setMensaje("EXISTE");
/*     */         try {
/* 109 */           respuesta = VerLista(respuesta, idLog, conexion);
/*     */         } catch (Exception ex) {
/* 111 */           respuesta.setEstado("ERROR");
/* 112 */           respuesta.setMensaje("INTERNAL #2");
/* 113 */           log.info("[" + idLog + "] LISTA - Error #2 [" + ex.getMessage() + "]");
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 128 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 130 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 134 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 120 */       if (respuesta.getEstado() == null) {
/* 121 */         respuesta.setEstado("ERROR");
/* 122 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 124 */       log.error("[" + idLog + "] LISTA - Error Process #3 [" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 128 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 130 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 135 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/* 137 */     ByteArrayOutputStream baos = new ByteArrayOutputStream();
/*     */     
/*     */ 
/*     */ 
/* 141 */     Gson gson = new Gson();
/* 142 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 144 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 146 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private String PrepararLista(String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 151 */     ResultSet resultSet = null;
/* 152 */     String query = null;
/* 153 */     String resultado = "";
/*     */     
/*     */     try
/*     */     {
/* 157 */       log.info("[" + idLog_ + "] Preparando la funcion [dba._WebS_Consulta_Lista]");
/*     */       
/* 159 */       this.stmt = conexion.createStatement();
/* 160 */       query = "SELECT dba._WebS_Consulta_Lista ('" + params.get("IMEI") + "') resultado";
/*     */       
/* 162 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 163 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 165 */       if (resultSet.next()) {
/* 166 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/* 167 */         resultado = resultSet.getString("resultado");
/* 168 */         conexion.commit();
/*     */       }
/*     */       else {
/* 171 */         resultado = "ERROR#INTERNAL #4";
/*     */       }
/*     */     }
/*     */     catch (Exception ex) {
/* 175 */       log.error("[" + idLog_ + "] LISTA - Error en el Procedimiento #5 [" + ex.getMessage() + "]");
/* 176 */       resultado = "ERROR#INTERNAL #5";
/*     */     }
/* 178 */     log.info("[" + idLog_ + "] respuesta [" + resultado + "]");
/* 179 */     return resultado;
/*     */   }
/*     */   
/*     */   private RespuestaLista VerLista(RespuestaLista respuesta, String idLog_, Connection conexion)
/*     */   {
/*     */     try
/*     */     {
/* 186 */       this.stmt = conexion.createStatement();
/* 187 */       String query = "SELECT * from dba.WebS_ConsultaLista where ConsultaNro = " + respuesta.getNroConsulta();
/*     */       
/*     */ 
/* 190 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 191 */       ResultSet resultSet = this.stmt.executeQuery(query);
/*     */       
/* 193 */       if (resultSet.next()) {
/*     */         do {
/* 195 */           DetalleLista detalleLista = new DetalleLista();
/* 196 */           detalleLista.setCodigoOperador(resultSet.getString("CodReferente"));
/* 197 */           detalleLista.setNombreApellidoOperador(this.StringUtils.ReplaceCE(resultSet.getString("NombresRef") + " " + resultSet.getString("ApellidosRef")));
/* 198 */           detalleLista.setCedulaCorreli(resultSet.getString("Cedula"));
/* 199 */           detalleLista.setNombreApellidoCorreli(this.StringUtils.ReplaceCE(new StringBuilder().append(resultSet.getString("Nombres")).append(" ").append(resultSet.getString("Apellidos")).toString()) + " (" + resultSet.getInt("ContReg") + "/" + resultSet.getInt("TotReg") + ")");
/* 200 */           detalleLista.setHabilitado(resultSet.getString("Habilitado"));
/* 201 */           detalleLista.setVoto(resultSet.getString("Voto"));
/* 202 */           detalleLista.setCelular(resultSet.getString("Celular"));
/* 203 */           detalleLista.setLocalVotacion(resultSet.getString("LocalVot"));
/* 204 */           detalleLista.setNroMesa(Integer.valueOf(resultSet.getInt("NroMesa")));
/* 205 */           detalleLista.setOrdenMesa(Integer.valueOf(resultSet.getInt("OrdenMesa")));
                    detalleLista.setAniversario((resultSet.getDate("Aniversario")));
/* 206 */           respuesta.getDetalleLista().add(detalleLista);
/* 207 */         } while (resultSet.next());
/*     */       }
/*     */       else {
/* 210 */         respuesta.setEstado("ERROR");
/* 211 */         respuesta.setMensaje("NO EXISTEN DATOS");
/*     */       }
/*     */     }
/*     */     catch (Exception ex) {
/* 215 */       log.error("[" + idLog_ + "] LISTA - Error en el Procedimiento #5 [" + ex.getMessage() + "]");
/* 216 */       respuesta.setEstado("ERROR");
/* 217 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }
/* 219 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 220 */     return respuesta;
/*     */   }
/*     */ }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\main\Lista.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */