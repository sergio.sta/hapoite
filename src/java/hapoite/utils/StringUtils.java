/*     */ package hapoite.utils;
/*     */ 
/*     */ import java.text.DecimalFormat;
/*     */ import java.text.NumberFormat;
/*     */ import java.util.StringTokenizer;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class StringUtils
/*     */ {
/*     */   public String padDouble(double number, int width, int dec)
/*     */   {
/*  27 */     NumberFormat formatter = new DecimalFormat("#########.##");
/*  28 */     String cadena = "" + formatter.format(number);
/*  29 */     StringTokenizer token = new StringTokenizer(cadena, ".");
/*  30 */     String entero = token.nextToken();
/*  31 */     String decimal = "";
/*  32 */     if (token.hasMoreTokens()) {
/*  33 */       decimal = token.nextToken();
/*     */     }
/*  35 */     int cont = width - entero.length() - dec;
/*  36 */     for (int i = 0; i < cont; i++) {
/*  37 */       entero = "0" + entero;
/*     */     }
/*  39 */     int cola = decimal.length();
/*  40 */     for (int j = 0; j < dec - cola; j++) {
/*  41 */       decimal = decimal + "0";
/*     */     }
/*  43 */     if (decimal.length() > 2) {
/*  44 */       decimal = decimal.substring(0, 2);
/*     */     }
/*     */     
/*  47 */     return entero + decimal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String ZeroPad(int number, int width)
/*     */   {
/*  60 */     StringBuffer result = new StringBuffer("");
/*     */     
/*  62 */     for (int i = 0; i < width - Integer.toString(number).length(); i++) {
/*  63 */       result.append("0");
/*     */     }
/*  65 */     result.append(Integer.toString(number));
/*     */     
/*  67 */     return result.toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String ZeroPad(double number, int width)
/*     */   {
/*  79 */     StringBuffer result = new StringBuffer("");
/*     */     
/*  81 */     for (int i = 0; i < width - Double.toString(number).length(); i++) {
/*  82 */       result.append("0");
/*     */     }
/*  84 */     result.append(Double.toString(number));
/*     */     
/*  86 */     return result.toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String padRight(String str, int width)
/*     */   {
/*     */     String s;
/*     */     
/*     */ 
/*     */      
/*     */     
/*     */ 
/*  99 */     if (str == null) {
/* 100 */       s = " ";
/*     */     } else {  
/* 102 */       if (str.equals("")) {
/* 103 */         s = " ";
/*     */       } else {
/* 105 */         s = str;
/*     */       }
/*     */     }
/* 108 */     return String.format("%1$-" + width + "s", new Object[] { s });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String padLeft(String str, int width)
/*     */   {
/* 119 */     return String.format("%1$#" + width + "s", new Object[] { str });
/*     */   }
/*     */   
/*     */   public String Pad(String str, String character, String direction, int width) {
/* 123 */     while (str.length() < width) {
/* 124 */       if (direction.equals("L")) {
/* 125 */         str = character + str;
/*     */       }
/*     */       else {
/* 128 */         str = str + character;
/*     */       }
/*     */     }
/*     */     
/* 132 */     return str;
/*     */   }
/*     */   
/*     */   public String ReplaceCE(String cadena)
/*     */   {
/* 137 */     cadena = cadena.replace("Á", "A");
/* 138 */     cadena = cadena.replace("É", "E");
/* 139 */     cadena = cadena.replace("Í", "I");
/* 140 */     cadena = cadena.replace("Ó", "O");
/* 141 */     cadena = cadena.replace("Ú", "U");
/* 142 */     cadena = cadena.replace("á", "a");
/* 143 */     cadena = cadena.replace("é", "e");
/* 144 */     cadena = cadena.replace("í", "i");
/* 145 */     cadena = cadena.replace("ó", "o");
/* 146 */     cadena = cadena.replace("ú", "u");
/* 147 */     cadena = cadena.replace("Ñ", "N");
/* 148 */     cadena = cadena.replace("ñ", "n");
/* 149 */     cadena = cadena.replace("/", "-");
/* 150 */     cadena = cadena.replace("º", "o.");
/*     */     
/*     */ 
/* 153 */     return cadena;
/*     */   }
/*     */ }

